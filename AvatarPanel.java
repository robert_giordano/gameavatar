//Robert Giordano
//Lab 6 25 November 2012
package Game;

import java.awt.*;
import java.util.*;
import javax.swing.*;

public class AvatarPanel extends JPanel implements Constants
{
	private static final long serialVersionUID = 1L;
	
	private ArrayList<Avatar> avatars;
	
	private final static Color[] colors = 
	{
		new Color(0xCC, 0xFF, 0x00),
		new Color(0xCC, 0xCC, 0x00),
		new Color(0xCC, 0x99, 0x00),
		new Color(0xCC, 0x66, 0x00),
		new Color(0xCC, 0x33, 0x00),
		new Color(0xCC, 0x00, 0x00),
	}; //color array to change Avatar from green to red as it dies
	
	int color;
	
	public AvatarPanel()
	{
		avatars = new ArrayList<Avatar>();
		setBorder(BorderFactory.createRaisedBevelBorder());
		setPreferredSize(new Dimension(1000, 30));
	}
	
	public @Override void paintComponent(Graphics g)
	{
		super.paintComponent(g);//call to the parent class to handle paint
		
		for (Avatar s: avatars)
		{
			s.draw(g);
		}
		
	}
	
	public void makeAvatar()
	{
		if (avatars.size() >= MAX_AVATARS) return;
		
		Avatar avatar = new Avatar(0, 0);
		Rectangle bounds = avatar.getBounds();
		double scale = 30.0 / bounds.height;
		avatar.setScale(scale);
		
		bounds = avatar.getBounds();
		avatar.setPosition(new Point (bounds.width * avatars.size() -bounds.x, -bounds.y));
		
		color = 0;
		if (avatars.isEmpty()) {avatar.setBackground(colors[color]);}
		avatars.add(avatar);	
		color = 0;
		repaint();
	}
	
	public boolean killAvatar()
	{
		avatars.remove(avatars.size() -1);
		repaint();
		return avatars.isEmpty();//Game over
	}
	
	public boolean setColor(boolean good)
	{
		if (good)
		{
			if (color > 0) color--;//good decrease color
			else return false;
		}
		else
		{
			if (color < colors.length -1) color++;//bad increase color
		}
		avatars.get(0).setBackground(colors[color]);
		repaint();
		return true;
	}
	
	public void reset() 
	{
		avatars.clear();
		int avs = GAME_START;
		for (int i = 0; i<avs; i++)
		{
			makeAvatar();
		}
	}
}// End AvatarPanel
  