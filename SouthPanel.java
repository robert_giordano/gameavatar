//Robert Giordano
//Lab6 25 November 2012
package Game;

import java.awt.event.*;
import javax.swing.*;

import java.awt.*;

public class SouthPanel extends JPanel implements Constants, ActionListener
{
	private static final long serialVersionUID = 1L;
	
	private JLabel label;
	private int level;
	private int points;
	private JButton plus;
	private JButton minus;
	private JButton reset;
	
	public SouthPanel()
	{
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		reset = new JButton("Reset");
		reset.setBackground(Color.BLUE);
		reset.setForeground(Color.WHITE);
		reset.setToolTipText("Reset the Game");
		reset.addActionListener(this);
		add(reset);
		
		add(Box.createHorizontalStrut(SEPARATOR));
		
		plus = new JButton( "+");
		plus.setBackground(Color.BLUE);
		plus.setForeground(Color.WHITE);
		plus.setToolTipText("Push to increases the game's difficulty level" + LEVELS[level]);
		plus.addActionListener(this);
		add(plus);
		
		add(Box.createHorizontalStrut(SEPARATOR));
		
		minus = new JButton( "-");
		minus.setBackground(Color.BLUE);
		minus.setForeground(Color.WHITE);
		minus.setToolTipText("Push to decrease the game's difficulty level" + LEVELS[level]);
		minus.addActionListener(this);
		add(minus);
		
		add(Box.createHorizontalStrut(SEPARATOR));
		add(avatarPanel);
		add(Box.createHorizontalGlue());
		
		label = new JLabel("Points:");
		label.setOpaque(true);
		label.setBackground(Color.BLUE);
		label.setForeground(Color.WHITE);
		label.setPreferredSize(new Dimension(100, 30));
		label.setBorder(BorderFactory.createEtchedBorder());
		add(label);	
	}
	
	public int getLevel() 
	{
		return level;
	}
	
	public void actionPerformed(ActionEvent event) 
	{
		points = 0;
		label.setText("Points:" + 0);
		
		Object o = event.getSource();
		JButton button = (JButton)o;
		if (button.getText().equals("+"))
		{
			if (getLevel()>=LEVELS.length-1)
			{
				Toolkit.getDefaultToolkit().beep();
			}
			else level++;	
		}
		else if (button.getText().equals("-"))
		{
			if (getLevel()<=0)
			{
				Toolkit.getDefaultToolkit().beep();
			}	
			else level--;
		}
		
		minus.setToolTipText("Push to decrease the game's difficulty level" + LEVELS[level]);
		plus.setToolTipText("Push to increases the game's difficulty level" + LEVELS[level]);
		
		avatarPanel.reset();
		background.reset(getLevel());
	}//End of actionPerformed
	
	public void adjustPoints(int points)
	{
		this.points += points;
		label.setText("Points: " + points);
	}//use this for whatever calls for points

}//End of SouthPanel build
