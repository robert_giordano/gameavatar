//Bob Giordano, 25 October 2012
//Background class for the sprite image
//Lab 6

package Game;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import javax.imageio.*;
import java.net.*;

public class Game extends JApplet
{
	private static final long serialVersionUID = 1L;
	private final Dimension APP_SIZE = new Dimension(1200, 800);
    private BufferedImage image;
    /** Initialize the JApplet */
    
    public @Override void init()
    { 
      getIcon("sprite.png");
      setIcon();

      JPanel panel = new MainPanel();//point to MainPanel instead of Background
      Container container = getContentPane();
      container.add(panel);
      setSize(APP_SIZE);
    }
  /** Method to load the Buffered image for the icon */
    
  private void getIcon(String picture)
  {  
	  try
	  {
         URL file = this.getClass().getResource("/data/" + picture);
         image = ImageIO.read(file);
	  }
	  catch (Exception e) {  JOptionPane.showMessageDialog(this, e);  }
  }
 
  private void setIcon()// Load the icon and set it into the current frame's title bar
  {  
	  Component root = SwingUtilities.getRoot(this);
	  if (root instanceof JFrame || root instanceof Frame)
	  {   Frame frame = (Frame)root;
         frame.setIconImage(image);
	  }
  }
}    // End of Game class