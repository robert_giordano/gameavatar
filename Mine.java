//Bob Giordano, 25 November 2012
//Lab6 Shield Build

package Game;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Ellipse2D;

public class Mine extends Sprite 
{
	int[] xPoints = {95, 130, 70}, yPoints = {25, 100,100};
	public Mine(int x, int y)
	{
		super();
		Ellipse2D.Float left = new Ellipse2D.Float(75, 100, 50, 50);
		add(left, Color.RED);
	
		setPosition(new Point(x, y));	
	}//Shield constructor

	public double getMinScaleFactor()
 	{
 		return MINSCALE;
 	}
	
	public double getMaxScaleFactor()
	{
		return MAXSCALE;
	}
}//End of Shield class
