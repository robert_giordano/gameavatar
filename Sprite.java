//Robert Giordano, 25 November 2012
// Lab 6 Sprite build

package Game;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.util.ArrayList;

public abstract class Sprite extends Area implements Constants
{
	private int x;  // horizontal & vertical position of a sprite
	private int y;
	private double angle, scale; // rotation angle and scale factor
	private ArrayList<Area> areas; // List of sprite pieces
	private ArrayList<Color> colors; // List of colors for each piece
	private Color background;
	private int oldX, oldY;
	private double oldScale, oldAngle;
	protected static final double MINSCALE = .25, MAXSCALE = .5;
	
	public Sprite()//constructor
	{
		areas = new ArrayList<Area>();
		colors = new ArrayList<Color>();
		oldAngle = angle = oldX = oldY = x = y = 0;
		oldScale = scale = 1.0;
		
		background = null;
	}
	
	public void setParameters() 
	{
		oldX = x;
		oldY = y;
		oldScale = scale;
		oldAngle = angle;
	}
	
	public void restore()
	{
		AffineTransform tx = getTransform(oldX, oldY, oldAngle, oldScale);
		transform(tx);  
	}
	
	public void setAngle(double angle)
	{
       AffineTransform tx = getTransform(x, y, angle, scale);
	   transform(tx);   
	}
	
	public double getAngle() 
	{
		return angle;
	}
	
	public void setScale(double scale) 
	{
		AffineTransform tx = getTransform(x, y, angle, scale);
		transform(tx);
	}
	
	public double getScale() 
	{
		return scale;
	}
	
	public void setPosition(Point p) 
	{
		AffineTransform tx = getTransform(p.x, p.y, angle, scale);
		transform(tx);
	}
	
	public Point getPosition() 
	{
		return new Point(x, y);
	}
	
	public void add(Shape shape, Color color) 
	{
		Area a = new Area(shape);
		super.add(a);
		colors.add(color);
		areas.add(a);	
	}
	
	public Color getBackground()
	{
		return background;
	}
	
	public void setBackground (Color background)
	{
		this.background = background;
	}
	
	private AffineTransform getTransform
	 (int x, int y, double angle, double scale)
	{ 
	  AffineTransform transform = new AffineTransform();
	  transform.translate(x, y);

	  double deltaA = angle - this.angle;
	  transform.rotate(deltaA * Math.PI/180.);

	  double deltaScale = scale/this.scale;
	  transform.scale(deltaScale, deltaScale);

	  Rectangle rect = getBounds();
	  int centerX = rect.x + rect.width/2;
	  int centerY = rect.y + rect.height/2;
	  transform.translate(-centerX, -centerY);

	  this.x = x;//now tell my sprite the new position
	  this.y = y;//now tell my sprite the new position
	  this.angle = angle;
	  this.scale = scale;

	  return transform;
	}
	
	public void draw(Graphics g) 
	{
	  Graphics2D g2D = (Graphics2D)g;//casting example
	  
	  if (background != null)
	  {
		  Rectangle bounds = getBounds();
		  g2D.setColor(background);
		  g2D.fill(bounds);
	  }
	  
	  for (int i=0; i<areas.size(); i++)
	  {
		  g2D.setColor(colors.get(i));
		  g2D.fill(areas.get(i));
	  }	
	}//end of draw method
	
	public @Override void transform(AffineTransform tx) 
	{
	   super.transform(tx);
	   
	   for (Area area : areas)
		   area.transform(tx);
	}
	
	public abstract double getMinScaleFactor();
	
	public abstract double getMaxScaleFactor();	
}//End of Sprite class
