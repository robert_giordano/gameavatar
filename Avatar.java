//Bob Giordano, 25 November 2012
//Lab 6 Avatar build 

package Game;

import java.awt.*;

public class Avatar extends Sprite implements Move, Cloneable
{
    public Avatar(int x, int y) 
    {
    	super();
    	int[] xpoints1 =  new int[]{85, 75, 15, 195};// fill the array
    	int[] ypoints1 =  new int[]{185, 85, 45, 85};// fill the array
    	
    	Polygon poly1  =  new Polygon(xpoints1,ypoints1,xpoints1.length);//Poly refers back to variable filling it with new
    	add(poly1, Color.CYAN);//Head constructor
    	
    	int[] xpoints2 =  new int[]{95, 85, 25, 205};// fill the array
    	int[] ypoints2 =  new int[]{185, 55, 15, 85};// fill the array  
    	
    	Polygon poly2  =  new Polygon(xpoints2,ypoints2,xpoints2.length);//Poly refers back to variable filling it with new
    	add(poly2, Color.darkGray);//Body constructor
    	
    	setPosition(new Point(x, y));
    }//End Avatar constructor
     
    public Point nextPosition()
    {
    	 Point p = getPosition();
    	 return p;
    }
     
 	public void moveFailed()
 	{
 		
 	}
 	
 	public double getMinScaleFactor()
 	{
 		return MINSCALE;
 	}
	
	public double getMaxScaleFactor()
	{
		return MAXSCALE;
	}	
}//end of Avatar class
