//Bob Giordano, Lab6
//25 November 2012

package Game;

import java.awt.*;
import java.awt.geom.Ellipse2D;

public class Predator extends Sprite  implements Move, Cloneable
{
	private static final double MINSCALE = 0.25, MAXSCALE = 1.75;
	private int direction;
	
	int[] xPoints = {95, 130, 70}, yPoints = {25, 100,100};
	
	public Predator(int x, int y)
	{
		Polygon poly = new Polygon(xPoints, yPoints, xPoints.length);
		add(poly, Color.BLUE);
	
		Rectangle rect = new Rectangle(75, 100, 50, 50);
		add(rect, Color.YELLOW);
	
		Ellipse2D.Float left = new Ellipse2D.Float(75, 75, 50, 50);
		add(left, Color.BLACK);
		
		direction = (int) (Math.random() * ANGLES.length);
		setAngle(ANGLES[direction]);
		setPosition(new Point(x,y));
	}// End Predator constructor
	
	public Point nextPosition()
    {
		Point point1 = getPosition();
    	point1.x += DELTA_XY[direction][0] * 3; //*3 Sets the speed of the Predator
    	point1.y += DELTA_XY[direction][1] * 3;
    	setAngle(ANGLES[direction]);
    	setPosition(point1);	
    	return point1;
    }
	
	public void moveFailed()
	{
		direction = (int) (Math.random() * ANGLES.length);	
		restore();
	}
	
	public Object clone()
 	{
		Point point1 = nextPosition();//getPosition
		Predator preds = new Predator(point1.x,  point1.y);
		preds.setAngle(getAngle());
		preds.setScale(getScale());
		return preds;	
 	}//predatorClone
	
	public double getMinScaleFactor()
 	{
 		return MINSCALE;
 	}
	
	public double getMaxScaleFactor()
	{
		return MAXSCALE;
	}
}//End of Predator build
