// Bob Giordano, 25 November 2012 
// Lab6 Game build

package Game;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;
import javax.swing.Timer;

import java.util.*;

public class Background extends JPanel implements Constants, ActionListener
{
	private static final long serialVersionUID = 1L;
	
	private final Color BACK_COLOR = Color.GREEN;
	private int SPEED = 50;// sets the speed control for Avatar
	private int TRIES = 2;
	private ArrayList<Sprite> sprites;//array list of sprites
	
	public Background()//start of constructor
	{
		sprites = new ArrayList<Sprite>();
		setBackground(BACK_COLOR);
		
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		manager.addKeyEventDispatcher(new KeyEventDispatcher()
		{
			public boolean dispatchKeyEvent(KeyEvent e)
			{
				Sprite sprite = sprites.get(0);
				Point p = sprite.getPosition();
				double angle = 0;
				switch (e.getKeyCode())
				{
					case KeyEvent.VK_DOWN: 
						p.y += SPEED;
						angle = 90;
						break;
						
					case KeyEvent.VK_UP:
						p.y -= SPEED;
						angle = 270;
						break;
					
					case KeyEvent.VK_LEFT: 
						p.x -= SPEED;
						angle = 180;
						break;
					
					case KeyEvent.VK_RIGHT:
						p.x += SPEED;
						angle = 10;
						break;
					}
				sprite.setParameters();
				sprite.setPosition(p);
				sprite.setAngle(angle);
				
					if (!getBounds().contains(sprite.getBounds()))
					{   
						Toolkit.getDefaultToolkit().beep();
						sprite.restore();
						return true;
					}
					repaint();
					return true;	
				}
			});
		
		Timer timer = new Timer(70, this);
		timer.start();//Start the timer.
		
		//Avatar avatar = (Avatar) sprites.get(1);
		//for (Sprite s : sprites);
		//{
			//if (s instanceof Predator)
				//if (avatar.intersects(s.getBounds())
						//{
					
						//}
		//}
		
		
		//add methods for bad or good here
		
	}//End of constructor
	
	public void actionPerformed(ActionEvent event)
	{
		Move move;
		for (Sprite s: sprites)
		{
			if (s instanceof Move)
			{
				for (int i = 0; i<TRIES; i++)
				{	
					move = (Move)s;
					move.nextPosition();
					if (!getBounds().contains(s.getBounds()))
					{
						move.moveFailed();
					}
					else 
					{
						s.setParameters();
						break;//gets me out of the loop.
					}	
				}	//add method for random move if hits (getBounds)instance of move?	
			}
		}
		repaint();
	}
	
	//Call makeAvatar() for new men here
	
	// set rules methods here as per lab6 instructions, use Area class for some methods(intersects)?
	
	public @Override void paintComponent(Graphics g)
	{
		super.paintComponent(g);// go gets paint and does the drawing for us.
		for (Sprite s: sprites)
		{
			if (s instanceof Move)
			{
				((Move)s).nextPosition();
			}
			s.draw(g);
		}
			
		for (int i = 0; i < sprites.size();i++)
		{
			sprites.get(i).draw(g);
		}
	}
	
	public void reset(int level)//
	{
		sprites.clear();
		
		int preds = PREDATORS[level];
		int mines = MINES[level];
		int shields = SHIELDS[level];
		int avs = 1;
		
		Sprite sprite;
		
		int x, y, angle;
		double min, max, scale;
		for (int i = 0; i < preds + avs + mines + shields; i++)
		{
			x = (int) (Math.random() * 600 + 100);//Keeps it out of corner.
			y = (int) (Math.random() * 400 + 100);//Keeps it out of corner.
				
			if (i >= preds + avs + mines) 
				sprite = new Shield (x, y);
			else if (i >= preds + avs)
				sprite = new Mine(x, y);
			else if (i >= avs)
				sprite = new Predator(x, y);
			else sprite = new Avatar (x, y);
				
			angle = (int) (Math.random()* 360);
			sprite.setAngle(angle);
				
			min = sprite.getMinScaleFactor();
			max = sprite.getMaxScaleFactor();
			scale = Math.random() * (max - min) + min;
			sprite.setScale(scale);
			sprites.add(sprite);
		}
		super.repaint();
	}//end of reset method
}//end of Background class
