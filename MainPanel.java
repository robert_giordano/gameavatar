//Robert Giordano
//Lab 6 25 November 2012
package Game;

import java.awt.*;
import javax.swing.*;

public class MainPanel extends JPanel implements Constants
{
	private static final long serialVersionUID = 1L;
	
	public MainPanel()
	{
		setLayout(new BorderLayout());
		add(background, BorderLayout.CENTER);
		add(southPanel, BorderLayout.SOUTH);
	}
}
