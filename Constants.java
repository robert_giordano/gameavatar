//Robert Giordano
//Lab 6 25 November 2012
package Game;

import java.awt.*;

public interface Constants 
{ 
	Dimension BUTTON_SIZE = new Dimension(30, 30); 
	int SEPARATOR   = 4;  // Separator width between GUI components

	String[] LEVELS = {"Easy", "Moderate", "Normal", "Difficult", "Megatron!"};

	int[] PREDATORS = { 1, 2, 3, 5, 7 }; // Predators/difficulty level
	int[] MINES     = { 2, 3, 4, 5, 6 }; // Mines/difficulty level
	int[] SHIELDS   = { 6, 5, 4, 3, 2 }; // Shields/difficulty level
	
	int MAX_AVATARS = 24; // The maximum number of avatars in waiting, fill the panel
	int GAME_START  = 4; // The number of avatars in panel to start game
	
	AvatarPanel avatarPanel  =  new AvatarPanel();
	Background  background   =  new Background();
	SouthPanel  southPanel   =  new SouthPanel();
}// end of Constants
