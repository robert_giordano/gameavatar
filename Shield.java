//Bob Giordano, 25 November 2012
//Lab6, Mine build

package Game;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;

public class Shield extends Sprite
{
	public Shield(int x, int y)
	{
		Rectangle rect = new Rectangle(25, 100, 50, 50);
		add(rect, Color.WHITE);

		setPosition(new Point(x, y));
		
	}//Mine constructor
	
	public double getMinScaleFactor()
 	{
 		return MINSCALE;
 	}
	
	public double getMaxScaleFactor()
	{
		return MAXSCALE;
	}
}// End of Mine class
